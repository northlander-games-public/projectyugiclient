package Interfaces 
{
	import Utils.Promise.Deferred;
	
	/**
	 * ...
	 * @author Daniel Murker
	 */
	public interface StateInterface extends UpdaterInterface
	{
		function init():Deferred;
		function enter(data:Object = null):void;
		function exit():void;
		
		/*protected function _setEventListeners():void;
		protected function _unsetEventListeners():void;*/
		
	}
	
}