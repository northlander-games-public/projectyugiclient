package NetworkCommands
{
	import Events.PostGameEvent;
	
	public class PostGameStateCommands {
		protected static var s_instance:PostGameStateCommands;
		
		public function PostGameStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO PostGameStateCommands!");
			}
		}
		
		public static function getInstance():PostGameStateCommands {
			if (!s_instance) {
				s_instance = new PostGameStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function endGame():Object {
			var result:Object = {
				event: PostGameEvent.BACK_TO_MAIN,
				data: {}
			};
			
			return result;
		}
	}
}

class SingletonEnforcer {}