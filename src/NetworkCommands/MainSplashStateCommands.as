package NetworkCommands
{
	import Events.MainSplashEvent;
	
	public class MainSplashStateCommands {
		protected static var s_instance:MainSplashStateCommands;
		
		public function MainSplashStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO MainMenuStateCommands!");
			}
		}
		
		public static function getInstance():MainSplashStateCommands {
			if (!s_instance) {
				s_instance = new MainSplashStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function enterGame():Object {
			var result:Object = {
				event: MainSplashEvent.ENTER_GAME,
				data: {}
			};
			
			return result;
		}
	}
}

class SingletonEnforcer {}