package NetworkCommands
{
	import Events.CharacterSelectEvent;
	
	public class CharacterSelectStateCommands
	{
		protected static var s_instance:CharacterSelectStateCommands;
		
		public function CharacterSelectStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO MainMenuStateCommands!");
			}
		}
		
		public static function getInstance():CharacterSelectStateCommands {
			if (!s_instance) {
				s_instance = new CharacterSelectStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function selectCharacter(charNum:int):Object {
			var result:Object = {
				event: CharacterSelectEvent.CHARACTER_SELECTED,
				socketName: 'GAME',
				data: {
					selectedCharacter: charNum
				}
			};
			
			return result;
		}
		
		public function selectPower():Object {
			var result:Object = {
				event: CharacterSelectEvent.POWER_SELECTED,
				socketName: 'GAME',
				data: {
					selectedPower: 1
				}
			};
			
			return result;
		}
		
		public function countdownComplete():Object {
			var result:Object = {
				event: CharacterSelectEvent.COUNTDOWN_COMPLETE,
				socketName: 'GAME',
				data: {}
			};
			
			return result;
		}

	}
}

class SingletonEnforcer {}