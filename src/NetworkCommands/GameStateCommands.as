package NetworkCommands
{
	import Events.GameStateEvent;
	
	public class GameStateCommands 
	{
		protected static var s_instance:GameStateCommands;
		
		public function GameStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO GameStateCommands!");
			}
		}
		
		public static function getInstance():GameStateCommands {
			if (!s_instance) {
				s_instance = new GameStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function seekPlayer():Object {
			var result:Object = {
				event: GameStateEvent.SEEK_PLAYER,
				data: {}
			};
			
			return result;
		}
		
		/**
		 * @param	data {coordinates: {x: #, y: #}, position: #}
		 * @return object
		 */
		public function updateCursorPosition(data:Object):Object {
			var result:Object = {
				socketName: 'GAME',
				event: GameStateEvent.MOVE_CURSOR,
				data: data
			};
			
			return result;
		}
		
		/**
		 * @param	data {coordinates: {x: #, y: #}, position: #}
		 * @return object
		 */
		public function rotateCursor(data:Object):Object {
			var result:Object = {
				socketName: 'GAME',
				event: GameStateEvent.ROTATE_CURSOR,
				data: data
			};
			
			return result;
		}
		
		public function endGame(isLoser:Boolean):Object {
			var result:Object = {
				event: GameStateEvent.GAME_OVER,
				data: {
					loser: isLoser
				}
			};
			
			return result;
		}
	}
}

class SingletonEnforcer {}