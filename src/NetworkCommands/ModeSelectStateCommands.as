package NetworkCommands
{
	import Events.ModeSelectEvent;
	
	public class ModeSelectStateCommands {
		protected static var s_instance:ModeSelectStateCommands;
		
		public function ModeSelectStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO ModeSelectStateCommands!");
			}
		}
		
		public static function getInstance():ModeSelectStateCommands {
			if (!s_instance) {
				s_instance = new ModeSelectStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		/**
		 * @todo moar code going here when ranked is enabled.
		 */
		public function searchForOpponent(searchType:String):Object {
			var result:Object = {
				event: ModeSelectEvent.SEARCH_FOR_OPPONENT,
				data: {
					searchType: 'random'
				}
			};
			
			return result;
		}
		
		
		public function acceptMatch():Object {
			var result:Object = {
				event: ModeSelectEvent.ACCEPT_MATCH,
				data: {}
			}
			
			return result;
		}
	}
}

class SingletonEnforcer {}