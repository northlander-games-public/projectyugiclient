package NetworkCommands
{
	import Events.StateMachineEvent;
	
	public class StateMachineCommands 
	{
		protected static var s_instance:StateMachineCommands;
		
		public function StateMachineCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO STATEMCAHINECOMMANDS");
			}
		}
		
		public static function getInstance():StateMachineCommands {
			if (!s_instance) {
				s_instance = new StateMachineCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function enterState(stateName:String, data:Object=null):Object {
			var result:Object = {
				event: StateMachineEvent.STATEMACHINE_CHANGESTATE,
				data: {
					state: stateName, 
					data: data
				}
			};
			
			return result;
		}
	}
}

class SingletonEnforcer {}