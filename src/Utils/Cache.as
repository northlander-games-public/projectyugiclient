/**
 * Cache.as - used as an asset caching mechanism.  Is a glorified loader, with a reference maintained in the game swf.
 * this will be a promise based system.
 * @author ...
 */

package Utils 
{
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.system.ApplicationDomain;
	import flash.net.URLRequest;
	import flash.display.Loader;
	
	import Utils.Promise.Promise;
	import Utils.Promise.Deferred;
	
	public class Cache 
	{	
		private static var s_assets:Object = {};
		
		public function Cache() {
			
		}
		
		public static function load(assetUrl:String, callback:Function = null ):Deferred {	
			if (!s_assets.hasOwnProperty(assetUrl)) {
trace("FIRST LOAD FOR " + assetUrl);
				var deferred:Deferred = new Deferred;
				var promise:Promise = deferred.promise;
				
				var loader:Loader = new Loader;
				//@TODO modify code such that I don't need to merge the domains.
				var ldrContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
					loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, arguments.callee);
					deferred.resolve();
				});
				loader.load(new URLRequest(assetUrl), ldrContext);
				
				s_assets[assetUrl] = deferred;
			}
			s_assets[assetUrl].then(callback);
			
			return s_assets[assetUrl];
		}
		 
		
	}
}