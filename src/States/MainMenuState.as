/**
 * @todo handle connection to server, notifying the socket server that the game state has been entered.
 * @todo handle merging/connecting of blocks.
 * @todo bombs
 * @todo diamonds.
 * 
 * @author Daniel Murker
 */

package States 
{
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.SoundManager;
	
	import Events.BaseEvent;
	import Events.MainMenuEvent;
	import Events.NetworkEvent;
	import Events.PopupEvent;
	import Events.StateMachineEvent;
	
	import Interfaces.StateInterface;
	
	import NetworkCommands.MainMenuStateCommands;
	
	import Popups.SearchingForOpponentPopup;
	
	import Utils.Cache;
	import Utils.EventBus;
	import Utils.Promise.Deferred;
	
	
	
	public class MainMenuState implements StateInterface {
		protected var m_initialized:Boolean;
		protected var m_asset:MovieClip;
		//public const INITIALIZED_EVENT:String = MainMenuEvent.MENU_STATE_INITIALIZED;
		
		public function init():Deferred {
			m_initialized = false;
			
			var cachePromise:Deferred = Cache.load('/assets/swf/mainmenu.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.mainMenuScreen'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_initialized = true;
			
		}
		
		public function isInitialized():Boolean {
			return m_initialized;
		}
		
		public function enter(data:Object = null):void {
			//InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_play, enterModeSelect);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_play, searchForRandomOpponent);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_settings, enterSettings);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_credits, enterCredits);
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		public function exit():void {
			//InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_play, enterModeSelect);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_play, searchForRandomOpponent);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_settings, enterSettings);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_credits, enterCredits);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		protected function enterModeSelect(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var commandObj:Object = MainMenuStateCommands.getInstance().enterModeSelect();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
		}
		
		protected function searchForRandomOpponent(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var newPopup:SearchingForOpponentPopup = new SearchingForOpponentPopup();
			newPopup.activate();
		}
		
		protected function enterSettings(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			EventBus.dispatch(new StateMachineEvent(StateMachineEvent.STATEMACHINE_CHANGESTATE), {
				statename: "Settings"
			});
		}
		
		protected function enterCredits(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			EventBus.dispatch(new StateMachineEvent(StateMachineEvent.STATEMACHINE_CHANGESTATE), {
				statename: "Credits"
			});
		}
		
		public function timerUpdate(delta:int):void {}
	}
	
	
		
}