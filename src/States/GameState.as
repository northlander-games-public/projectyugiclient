/**
 * @todo handle connection to server, notifying the socket server that the game state has been entered.
 * @todo handle merging/connecting of blocks.
 * @todo bombs
 * @todo diamonds.
 * 
 * @author Daniel Murker
 */

package States 
{
	import flash.display.MovieClip;
	import Utils.Promise.Deferred;
	import flash.utils.getDefinitionByName;
	
	import Interfaces.StateInterface;
	
	import Utils.Cache;
	import Utils.EventBus;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.Updater;
	
	import Events.BaseEvent;
	import Events.GameStateEvent;
	import Events.NetworkEvent;
	import Events.PopupEvent;
	import Events.StateMachineEvent;
	
	import GameObjects.gameBlock;
	import GameObjects.gameCursor;
	
	import Popups.GameStartPopup;
	import Popups.GameOverPopup;
	
	import NetworkCommands.GameStateCommands;
	
	public class GameState implements StateInterface 
	{
		protected var m_asset:MovieClip;
		private var m_sourceBlock:gameBlock; // sourceBlock from which all measurements are derived
		
		private const BOARD_WIDTH:int = 10;
		private const BOARD_HEIGHT:int = 10;		
		protected var m_gameOverPopup:GameOverPopup = null;
		protected var m_gameStartPopup:GameStartPopup = null;
		
		//public const INITIALIZED_EVENT:String = GameStateEvent.GAME_STATE_INITIALIZED;
		
		protected var m_players:Vector.<Player> = new Vector.<Player>;
		
		protected var m_numTiles:int = 0;
		
		public function GameState() {
			//nothing here yet?
		}
		
		public function init():Deferred {
			
			m_gameOverPopup = new GameOverPopup();
			m_gameStartPopup = new GameStartPopup();
			
			var cachePromise:Deferred = Cache.load('/assets/swf/gameboard.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		/**
		 * this needs to be fixed once the other board becomes active
		 */
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.gameBoard'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_sourceBlock = new gameBlock('blue');
			
			//EventBus.dispatch(new GameStateINITIALIZED_EVENT));
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().pause();
			
			initializePlayers();
			
			/**
			 * listen to input events.
			 */
			InputManager.getInstance().register(InputManager.INPUT_LEFT, moveCursor); 
			InputManager.getInstance().register(InputManager.INPUT_RIGHT, moveCursor);
			InputManager.getInstance().register(InputManager.INPUT_DOWN, moveCursor); 
			InputManager.getInstance().register(InputManager.INPUT_UP, moveCursor);
			InputManager.getInstance().register(InputManager.INPUT_ROTATE_LEFT, rotateCursor); 
			InputManager.getInstance().register(InputManager.INPUT_ROTATE_RIGHT, rotateCursor);
			
			/**
			 * listen to network events
			 */
			EventBus.listen(GameStateEvent.SERVER_TILES_SET, replenishChunkList);
			
			/**
			 * add the state to the display, and initialize the board.
			 */
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			/**
			 * wait for the signal to start game
			 */
			EventBus.listen(GameStateEvent.GAME_START, handleGameStart);
			EventBus.listen(GameStateEvent.GAME_OVER, handleGameEnd);
			EventBus.listen(GameStateEvent.MOVE_CURSOR, networkMoveCursor);
			EventBus.listen(GameStateEvent.ROTATE_CURSOR, networkRotateCursor);
			EventBus.listen(GameStateEvent.LOCK_TILES, networkLockTiles);
			EventBus.listen(GameStateEvent.REMOVE_TILES, networkRemoveTiles);
			
			//powers
			EventBus.listen(GameStateEvent.POWER_DARKNESS_BEGINS, handleDarknessBegins);
			EventBus.listen(GameStateEvent.POWER_DARKNESS_ENDS, handleDarknessEnds);
			EventBus.listen(GameStateEvent.POWER_LIGHTNING_STRIKE, handleLightningStrike);
			EventBus.listen(GameStateEvent.POWER_LOCKDOWN, handleLockdown);
			EventBus.listen(GameStateEvent.POWER_UNLOCK, handleUnlock);
			EventBus.listen(GameStateEvent.POWER_WEB_SHOT, handleWebShot);
			EventBus.listen(GameStateEvent.POWER_WEB_SHOT_END, handleWebshotEnd);
			EventBus.listen(GameStateEvent.POWER_WEB_STUCK, handleStuck);
			
			
			m_gameStartPopup.activate();
			
		}
		
		/**
		 * clean up event listeners etc.
		 */
		public function exit():void {
			/**
			 * stop listening to network events
			 */
			EventBus.stopListening(GameStateEvent.SERVER_TILES_SET, replenishChunkList);
			
			/**
			 * stop listening to input events.
			 */
			InputManager.getInstance().unregister(InputManager.INPUT_UP, moveCursor);
			InputManager.getInstance().unregister(InputManager.INPUT_LEFT, moveCursor); 
			InputManager.getInstance().unregister(InputManager.INPUT_RIGHT, moveCursor);
			InputManager.getInstance().unregister(InputManager.INPUT_DOWN, moveCursor); 
			InputManager.getInstance().unregister(InputManager.INPUT_ROTATE_LEFT, rotateCursor); 
			InputManager.getInstance().unregister(InputManager.INPUT_ROTATE_RIGHT, rotateCursor);
			
			/**
			 * stop listening to event bus stuff
			 */
			EventBus.stopListening(GameStateEvent.GAME_START, handleGameStart);
			EventBus.stopListening(GameStateEvent.GAME_OVER, handleGameEnd);
			EventBus.stopListening(GameStateEvent.MOVE_CURSOR, networkMoveCursor);
			EventBus.stopListening(GameStateEvent.ROTATE_CURSOR, networkRotateCursor);
			EventBus.stopListening(GameStateEvent.LOCK_TILES, networkLockTiles);
			EventBus.stopListening(GameStateEvent.REMOVE_TILES, networkRemoveTiles);
			
			EventBus.stopListening(GameStateEvent.POWER_DARKNESS_BEGINS, handleDarknessBegins);
			EventBus.stopListening(GameStateEvent.POWER_DARKNESS_ENDS, handleDarknessEnds);
			EventBus.stopListening(GameStateEvent.POWER_LIGHTNING_STRIKE, handleLightningStrike);
			EventBus.stopListening(GameStateEvent.POWER_UNLOCK, handleUnlock);
			EventBus.stopListening(GameStateEvent.POWER_WEB_SHOT, handleWebShot);
			EventBus.stopListening(GameStateEvent.POWER_WEB_SHOT_END, handleWebshotEnd);
			EventBus.stopListening(GameStateEvent.POWER_WEB_STUCK, handleStuck);
			
			/**
			 * remove state from display
			 */
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
			//gotta love the fact that as3 has no 'removeallChildren' method for movieclips
			for (var playerIndex:int = 0; playerIndex <  m_players.length; playerIndex++ ) {
				m_players[playerIndex].resetBoard();
			}
			
			m_players = new Vector.<Player>;
			
			InputManager.getInstance().unpause();
		}
		
		public function initializePlayers():void {
			for (var i:int = 0; i < 2; i++) {
				var whichPlayer:int = i + 1;
				m_players.push(new Player(m_asset['board' + whichPlayer], m_sourceBlock));
			}
		}

		protected function handleGameStart(e:GameStateEvent):void {
			InputManager.getInstance().unpause();
		}
		
		protected function handleGameEnd(e:BaseEvent):void {
			InputManager.getInstance().pause();
			var eventData:Object = e.getData();
			var isLoser:Boolean = eventData.loser || false;
			m_gameOverPopup.setLoserState(isLoser);
			m_gameOverPopup.activate();
		}
		
		protected function replenishChunkList(e:BaseEvent):void {
			var data:Object = e.getData();
			var color:String = data.color;
			var column:int = data.column;
			var whichPlayer:int = data.player;
			
			m_players[whichPlayer].addTile(color, column);
		}
		
		/**
		 * Network Event Handlers
		 */
		protected function networkRotateCursor(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].rotate(eventData.direction, eventData.tweenTime, eventData.position);
		}
		
		protected function networkLockTiles(e:BaseEvent):void {
			var eventData:Object = e.getData();
			
			//var whichPlayer:int = eventData.player ? 0 : 1;
			m_players[eventData.player].lockTiles(eventData.tileList);
		}
		
		protected function networkRemoveTiles(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].removeTiles(eventData.tileList, eventData.tweenTime);
		}
		
		protected function networkMoveCursor(e:BaseEvent):void {
			/*var data:Object = e.getData();
			moveCursor(data.direction, true);*/
			var eventData:Object = e.getData();
			m_players[eventData.player].adjustVertexPosition(eventData.position);
		}
		
				
		/**
		 * INPUT HANDLERS
		 */
		
		protected function moveCursor(direction:String):void {
			sendNetworkUpdate('move', direction);
		}
		
		/**
		 * @todo evaluate whether this should be handled partially locally like move is, or if this is okay.
		 * @param	direction
		 */
		protected function rotateCursor(direction:String):void {
			sendNetworkUpdate('rotate', direction);
		}
		
		/**
		 * Send network events
		 */
		protected function sendNetworkUpdate(type:String, data:* = '' ):void {
			var commandObj:Object;
			
			var commandData:Object = {};
			
			switch(type) {
				case 'move':
					commandData['direction'] = data;
					commandData['position'] = m_players[0].getVertexPosition();
					commandObj = GameStateCommands.getInstance().updateCursorPosition(commandData);
					break;
				case 'rotate':
					commandData['direction'] = data;
					commandData['position'] = m_players[0].getVertexPosition();
					commandObj = GameStateCommands.getInstance().rotateCursor(commandData);
					break;
			}
			
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
		}
		
		public function timerUpdate(delta:int):void { }
	
		/**
		 * Powers
		 */
		protected function handleDarknessBegins(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].beginDarkness();
		}
		
		protected function handleDarknessEnds(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].endDarkness();
		}
		
		protected function handleLightningStrike(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].lightningStrike(eventData.col);
		}
		
		protected function handleLockdown(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].lockDownTile(eventData.coords);
		}
		
		protected function handleUnlock(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].unlockTile(eventData.coords);
		}
		
		protected function handleWebShot(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].webShot(eventData.coords);
		}
		
		protected function handleWebshotEnd(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].webShotEnd();
		}
		
		protected function handleStuck(e:BaseEvent):void {
			var eventData:Object = e.getData();
			m_players[eventData.player].struggle();
		}
	}
}

import com.greensock.TweenLite;
import com.greensock.easing.Linear;

import flash.display.MovieClip;

import Core.InputManager;

import GameObjects.gameBlock;
import GameObjects.gameCursor;

import Utils.Promise.Deferred;

class Player {
	private var m_board:MovieClip;
	private var m_cursor:gameCursor = new gameCursor();
	private var m_sourceBlock:gameBlock;
	
	private var m_gridMap:Array = [];
	private var m_vertexPosition:Array = [0,0];
	
	private var m_commandQueue:Array = [];
	private var m_commandQueueProcessing:Boolean = false;
	
	public function Player(board:MovieClip, sourceBlock:gameBlock) {
		m_board = board;
		
		m_board.hexMap.addChild(m_cursor.getAsset());
		
		m_sourceBlock = sourceBlock;
				
		m_cursor.getAsset().x = sourceBlock.getAsset().width;
		m_cursor.getAsset().y = sourceBlock.getAsset().height * 8;
	}
	
	public function getVertexPosition():Array {
		return m_vertexPosition.slice(0);
	}
	
	public function addTile(color:String, col:int):void {
		var targetX:int;
		var targetY:int;
		var block:gameBlock = new gameBlock(color);
		
		if (!m_gridMap[col]) {
			m_gridMap[col] = [];
		}
		
		m_gridMap[col].push(block);
		targetX = (m_sourceBlock.getAsset().width * col * (3/4));
		
		var row:int = m_gridMap[col].length - 1
		var vertOffset:int = m_sourceBlock.getAsset().height / 2;
		targetY = (8 - Math.floor(row)) * m_sourceBlock.getAsset().height;
		
		if (col % 2 == 0) { //even column
			targetY -= vertOffset;
		}
		
		var startingY:int = 0;
		if (col % 2 == 0) {
			startingY += (m_sourceBlock.getAsset().height / 2);
		}
		
		block.getAsset().x = targetX;
		block.getAsset().y = targetY;
		
		if (targetY >= startingY) {
			var childIndex:int = m_board.hexMap.getChildIndex(m_cursor.getAsset());
			m_board.hexMap.addChildAt(block.getAsset(), childIndex);
		}
	}
	
	private function getTilesAroundVertex(coords:Array):Array {
		for (var index:int = 0; index < coords.length; index++) {
			coords[index] = parseInt(coords[index]);
		}
		
		var biggerCol:int;
		var smallerCol:int;
		
		if (coords[0] % 2 == coords[1] % 2) {
			//column with two tiles is on the right
			biggerCol = coords[0] + 1;
			smallerCol = coords[0];
		} else {
			//column with two tiles is on the left
			biggerCol = coords[0];				
			smallerCol = coords[0] + 1;
		}
		
		var middleTile:int = coords[1] % 2 ? Math.ceil(coords[1] / 2) : Math.floor(coords[1] / 2);
		
		var lowerTileIndex:int = Math.floor(coords[1] / 2);
		
		var resultTiles:Array = [
			[biggerCol, lowerTileIndex],
			[smallerCol, middleTile],
			[biggerCol, lowerTileIndex + 1]
		];
		
		return resultTiles;
	}
	
	private function resolveRotation(direction:String, coords:Array):Boolean {
		if(coords.length > 2){
			for(var i:int = 0; i < coords.length; i+=2){
				resolveRotation(direction, [coords[i], coords[i+1]]);
			}
			return true;
		}
		
		
		var hexRotation:String = direction;
		var movingTiles:Array = getTilesAroundVertex(coords);
		
		
		
		//the rotation sequence is flipped later on if the 2 big hex col is left.
		if (coords[0] % 2 != coords[1] % 2) {
			hexRotation = (hexRotation == InputManager.INPUT_ROTATE_LEFT) ? InputManager.INPUT_ROTATE_RIGHT : InputManager.INPUT_ROTATE_LEFT; 
		}
					
		/**
		 * abstraction... awesome till it makes your eyes bleed.  --DM
		 */
		var temp:gameBlock;
		switch(hexRotation) {
			case InputManager.INPUT_ROTATE_LEFT:
				temp = m_gridMap[movingTiles[0][0]][movingTiles[0][1]];
				m_gridMap[movingTiles[0][0]][movingTiles[0][1]] = m_gridMap[movingTiles[1][0]][movingTiles[1][1]]; 
				m_gridMap[movingTiles[1][0]][movingTiles[1][1]] = m_gridMap[movingTiles[2][0]][movingTiles[2][1]];
				m_gridMap[movingTiles[2][0]][movingTiles[2][1]] = temp;
				break;
			case InputManager.INPUT_ROTATE_RIGHT:
				temp = m_gridMap[movingTiles[0][0]][movingTiles[0][1]];
				m_gridMap[movingTiles[0][0]][movingTiles[0][1]] = m_gridMap[movingTiles[2][0]][movingTiles[2][1]];
				m_gridMap[movingTiles[2][0]][movingTiles[2][1]] = m_gridMap[movingTiles[1][0]][movingTiles[1][1]];
				m_gridMap[movingTiles[1][0]][movingTiles[1][1]] = temp;
				
				break;
		}
		
		return true;
	}
	
	protected function rotateCursor(tweenTime:Number, direction:String):Deferred {
		var resultPromise:Deferred = new Deferred();
		
		TweenLite.to(m_cursor.getAsset(), tweenTime, { 
			ease: Linear.easeNone,
			//using string instead of num forces tweenlite to use relative vals. --DM
			rotation: direction == InputManager.INPUT_ROTATE_RIGHT ? "120" : "-120", 
			onCompleteParams: [
				resultPromise
			],
			onComplete: function(resultPromise:Deferred):void {
				resultPromise.resolve();
			}
		} );
		
		return resultPromise;
	}
	
	
	protected function settleTiles(tweenTime:Number = 0.5):Deferred {
		var promiseList:Vector.<Deferred> = new Vector.<Deferred>;
		var resultPromise:Deferred = new Deferred();
		
		m_gridMap.forEach(function(curColumn:Array, colIndex:int, map:Array ):void {
			curColumn.forEach(function(testTile:gameBlock, tileIndex:int, col:Array):void {
				var vertOffset:int = m_sourceBlock.getAsset().height / 2;
				var targetY:int = (8 - Math.floor(tileIndex)) * m_sourceBlock.getAsset().height;				
				if (colIndex % 2 == 0) { //even column
				//if(testTile.getAsset().x % m_sourceBlock.getAsset().width == 0) {//can't depend on odd index due to how blocks can be removed.
					targetY -= vertOffset;
				}
				
				var targetX:int =  m_sourceBlock.getAsset().width * colIndex * (3 / 4);
				
				
				if ( testTile.getAsset().x != targetX || testTile.getAsset().y != targetY) {
					var addedPromise:Deferred = new Deferred();
					promiseList.push(addedPromise);
					addedPromise.then(function():void {
						promiseList.splice(promiseList.indexOf((promiseList as Deferred)), 1);
						if (promiseList.length <= 0) {
							resultPromise.resolve();
						}
					});
					
					if (targetY < 0) {
						//if the piece isn't going to be on the board after moving, no sense in trying to tween the movement.
						testTile.getAsset().y = targetY;
						addedPromise.resolve();
					} else {
						TweenLite.to(testTile.getAsset(), tweenTime, { 
							ease: Linear.easeNone,
							y: targetY,
							x: targetX,
							onUpdateParams: [
								m_gridMap,
								testTile,
								colIndex,
							],
							onUpdate: function(map:Array, movingChunk:gameBlock, colIndex:int):void {
								var startingY:int = 0;
								
								if (colIndex % 2 == 0) {
									startingY += (movingChunk.getAsset().height / 2);
								}
								
								var currentPosition:int = movingChunk.getAsset().y + movingChunk.getAsset().height;
								if (currentPosition > startingY && !movingChunk.getAsset().parent) {
									m_board.hexMap.addChildAt(movingChunk.getAsset(), m_board.hexMap.numChildren - 1);
								}
							},
							onCompleteParams: [
								addedPromise
							],
							onComplete: function(addedPromise:Deferred):void {
								addedPromise.resolve();
							}
						} );

					}
				}
			});	
		});	
		
		return resultPromise;
	}
	
	/**
	 * lock a set of tiles determined by the server, represented by an array of coordStrings
	 * @param	tileList
	 */
	public function lockTiles(tileList:Array):void {
		m_commandQueue.push( {
			command: 'lock',
			tileList: tileList
		});
		
		processCommandQueue();
	}
	
	public function removeTiles(tileList:Array, tweenTime:Number):void {
		m_commandQueue.push( {
			command: 'remove',
			tileList: tileList,
			tweenTime: tweenTime
		});
		
		processCommandQueue();
	}
	
	public function rotate(direction:String, tweenTime:Number, vertexPosition:Array ):void {
		m_commandQueue.push( { 
			'command': 'rotate',
			'direction': direction,
			'tweenTime': tweenTime,
			'vertexPosition': vertexPosition 
		});
		
		processCommandQueue();
	}
	
	public function adjustVertexPosition(coords:Array):void {
		var coordX:int = +coords[0];
		var coordY:int = +coords[1];
		m_commandQueue.push( { 
			'command': 'move',
			'adjustment': {
				x: coordX,
				y: coordY
			}
		});
		
		processCommandQueue();
	}
	
	private function processCommandQueue(invoker:Function = null):void {
		if (m_commandQueue.length == 0 || (m_commandQueueProcessing == true && invoker != processCommandQueue)) {
			return;
		}
		InputManager.getInstance().pause();
		m_commandQueueProcessing = true;
		
		var curQueuePromise:Deferred;
		
		var curQueueCommand:Object = m_commandQueue.shift();
		
		switch (curQueueCommand.command) {
			case 'move':
				curQueuePromise = commandMove(curQueueCommand.adjustment.x, curQueueCommand.adjustment.y);
				break;
			case 'rotate':
				curQueuePromise = commandRotate(curQueueCommand.direction, curQueueCommand.tweenTime, curQueueCommand.vertexPosition);
				break;
			case 'lock':
				curQueuePromise = commandLock(curQueueCommand.tileList);
				break;
			case 'remove':
				curQueuePromise = commandRemove(curQueueCommand.tileList, curQueueCommand.tweenTime);
				break;
		}
		
		curQueuePromise.then(function():void {
			m_commandQueueProcessing = false;
			if (m_commandQueue.length > 0) {
				processCommandQueue(processCommandQueue);
			} else {
				InputManager.getInstance().unpause();
			}
		});
	}
	
	/**
	 * begin command functions
	 */
	private function commandMove(x:int, y:int ):Deferred {
		var movePromise:Deferred = new Deferred();
		m_vertexPosition[0] = x;
		m_vertexPosition[1] = y;
		
		var height:Number = m_sourceBlock.getAsset().height;
		var width:Number = m_sourceBlock.getAsset().width;
		
		var offsetFactor:Number;
		var offset:Number;
		
		//handle actual position math
		if (!(m_vertexPosition[1] % 2)) {
			m_cursor.getAsset().x = ((m_vertexPosition[0] + 1) * width);
			offsetFactor = Math.ceil((m_vertexPosition[0]) / 2);
			offset = width / 2;
			m_cursor.getAsset().x -= offset * offsetFactor;
		} else {
			m_cursor.getAsset().x = ((m_vertexPosition[0] + 1) * width * .75);
			
			if ((m_vertexPosition[0] % 2)) {
				m_cursor.getAsset().x += width * .25;
			}
		}
		
		m_cursor.getAsset().y = (height * 8) - ((m_vertexPosition[1]) * height / 2);
		
		//handle the graphical orientation of the cursor... blurg this code is wierd to read, but is more readable than if it were all one if condition
		if ((m_vertexPosition[0] % 2 !== m_vertexPosition[1] % 2) && m_cursor.getAsset().scaleX > 0) {
			m_cursor.getAsset().scaleX *= -1;
		} else if (m_cursor.getAsset().scaleX < 0) {
			m_cursor.getAsset().scaleX *= -1;
		}	
		
		movePromise.resolve();
		
		return movePromise;
	}
	
	private function commandRotate(direction:String, tweenTime:Number, vertexPosition:Array):Deferred {
		var rotationAttempt:Boolean = resolveRotation(direction, vertexPosition);
		var settlePromise:Deferred = settleTiles(tweenTime);
		var cursorRotatePromise:Deferred = rotateCursor(tweenTime, direction);
		
		var allPromise:Deferred = new Deferred();
		
		/**
		 * @TODO create a mechanism to do this in an abstract way.  current promise library has no deferred.all I don't think 
		 */
		settlePromise.then(function():void {
			if (cursorRotatePromise.resolved) {
				allPromise.resolve();
			}
		});
		
		cursorRotatePromise.then(function():void {
			if (settlePromise.resolved) {
				allPromise.resolve();
			}
		});
		
		return allPromise;

		//return cursorRotatePromise;
	}
	
	private function commandLock(tileList:Array):Deferred {
		var lockPromise:Deferred = new Deferred();
		
		for (var i:int = 0; i < tileList.length; i++) {
			var coords:Array = tileList[i].split(',');
			
			
			var yCoord:int = +coords[1];
			
			if(yCoord < m_gridMap[coords[0]].length){
				m_gridMap[coords[0]][coords[1]].lock();
				m_gridMap[coords[0]][coords[1]].getAsset().alpha = 0.5;
			}
				
			
		}
		
		lockPromise.resolve();
		return lockPromise;
	}
	
	/**
	 * rewritten copy of resolveMatches, hopefully like before, bruteforcing will solve a lot of the issues.
	 * @param	whichPlayer int, 1 for local player, 2 for remote.
	 * @param	matchList - see detect matches for how this is handled.
	 */
	private function commandRemove(tileList:Array, tweenTime:Number):Deferred {
		var i:int, j:int;
		var remainingTiles:int = -1;
		
		//iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
		m_gridMap.forEach(function(curColumn:Array, colIndex:int, map:Array ):void { 
			m_gridMap[colIndex] = curColumn.filter(function(testTile:gameBlock, tileIndex:int, col:Array):Boolean {				
				var result:Boolean = true;
				
				var coords:String = colIndex + ',' + tileIndex;

				if (~tileList.indexOf(coords)) {
					result = false;
					m_board.hexMap.removeChild(testTile.getAsset());
				}
				
				return result;
			});
			
			curColumn = m_gridMap[colIndex];
		});
			
		var settlePromise:Deferred = settleTiles(tweenTime);
		return settlePromise;
	}
	
	/**
	 * remove all tiles for the board at end of game.
	 */
	public function resetBoard():void {
		var tiles:Array = [];
		
		m_gridMap.forEach(function(curColumn:Array, colIndex:int, map:Array ):void { 
			m_gridMap[colIndex] = curColumn.forEach(function(testTile:gameBlock, tileIndex:int, col:Array):void {
				tiles.push(testTile.getAsset());
			});
		});
		
		var childIndex:int = 0;
		while (m_board.hexMap.numChildren > childIndex) {
			if(tiles.indexOf(m_board.hexMap.getChildAt(childIndex))) {
				m_board.hexMap.removeChildAt(childIndex);	
			} else {
				childIndex++;
			}
			
		}
		
		/*m_gridMap.forEach(function(curColumn:Array, colIndex:int, map:Array ):void { 
			m_gridMap[colIndex] = curColumn.forEach(function(testTile:gameBlock, tileIndex:int, col:Array):Boolean {
				var hasChild:Boolean = m_board.contains(testTile.getAsset());
				if(hasChild){
					m_board.hexMap.removeChild(testTile.getAsset());	
				}
				
			});
		});*/
		
		//m_board.hexMap.removeChild(m_cursor.getAsset());
	}
	
	/**
	 * Begin POwers
	 */
	
	public function beginDarkness():void {
		trace("BEGIN DARKNESS");
	}
	
	public function endDarkness():void {
		trace("END DARKNESS");
	}

	public function lightningStrike(col:int):void {
		trace("LIGHTNING");
	}
		
	public function unlockTile(coords:Array):void {
		trace ("UNLOCK");
	}
	
	public function webShot(coords:Array):void {
		trace("WEBSHOT");
	}

	public function webShotEnd():void {
		trace("END WEBSHOT");
	}

	public function struggle():void {
		trace("STRUGGLE");
	}
	
	 /**
	  * end powers
	  */
}