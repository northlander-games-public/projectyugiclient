/**
 * @author Daniel Murker
 * 
 * Character Select Screen
 */

package States { 
	import Events.BaseEvent;
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import flash.text.TextFieldType;
	
	import Interfaces.StateInterface;
	
	import Utils.Cache;
	import Utils.Promise.Deferred;
	import Utils.EventBus;
	
	import Core.InputManager;
	import Core.DisplayManager;
	import Core.SoundManager;
	import Core.Updater;
	
	import Events.NetworkEvent;
	import Events.CharacterSelectEvent;
	import NetworkCommands.CharacterSelectStateCommands;
	
	public class CharacterSelectState implements StateInterface {
		protected var m_asset:MovieClip;
		
		protected var m_delta:int;
		protected var m_updateInterval:int;
		protected var m_intervalCount:int;
		
		public function init():Deferred {
			var cachePromise:Deferred = Cache.load('/assets/swf/characterSelect.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.characterSelect'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			
		}
		
		public function enter(data:Object = null):void {
			m_delta = 0;
			m_updateInterval = 1000;
			m_intervalCount = 5;
			
			m_asset.characterSelectWrapper.visible = true;
			//m_asset.powerSelectWrapper.visible = false;
			m_asset.champPortrait1.visible = false;
			m_asset.champPortrait2.visible = false;
			/*m_asset.selectedPower1.visible = false;
			m_asset.selectedPower2.visible = false;*/
			m_asset.descriptionField.visible = false;
			m_asset.countDownField.visible = false;
			
			m_asset.characterName1.text = '';
			m_asset.characterName2.text = '';
			
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_1, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_2, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_3, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_4, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_5, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_6, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_7, selectCharacter);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_8, selectCharacter);
			
			EventBus.listen(CharacterSelectEvent.CHARACTER_ACKNOWLEDGED, acknowledgeCharacter);
			EventBus.listen(CharacterSelectEvent.CHARACTER_SELECTED, netSelectCharacter);
			
			
			
			
			EventBus.listen(CharacterSelectEvent.BEGIN_COUNTDOWN, beginCountdown);
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		public function exit():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_1, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_2, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_3, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_4, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_5, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_6, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_7, selectCharacter);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.characterSelectWrapper.btn_character_8, selectCharacter);
			
			EventBus.stopListening(CharacterSelectEvent.CHARACTER_ACKNOWLEDGED, acknowledgeCharacter);
			EventBus.stopListening(CharacterSelectEvent.CHARACTER_SELECTED, netSelectCharacter);
			
			EventBus.stopListening(CharacterSelectEvent.BEGIN_COUNTDOWN, beginCountdown);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		
		/**
		 * local character and power select
		 */
		
		 
		protected function selectCharacter(target:Object):void { 
			var charNum:int = +(target.name.substr('btn_character_'.length));
			var commandObj:Object = CharacterSelectStateCommands.getInstance().selectCharacter(charNum);
			
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
		}
		
		
		
		/**
		 * server acknowledges our selection of character and power
		 */
		protected function acknowledgeCharacter(event:BaseEvent):void {
			var eventData:Object = event.getData();
			m_asset.champPortrait1.visible = true;
			m_asset.characterName1.text = eventData.name;
		}
		
		/**
		 * remote character and power select
		 */
		protected function netSelectCharacter(event:BaseEvent):void {
			var eventData:Object = event.getData();
			m_asset.champPortrait2.visible = true;
			m_asset.characterName2.text = eventData.name;
		}
		
		/*protected function netSelectPower(event:BaseEvent):void {
			m_asset.selectedPower2.visible = true;
			m_asset.selectedPower2.stage.invalidate();
		}*/
		
		
		/**
		 * both players ready, begin countdown
		 */
		protected function beginCountdown(e:BaseEvent):void {
			m_asset.countDownField.embedFonts = false;
			m_asset.countDownField.text = m_intervalCount;
			m_asset.countDownField.visible = true;
			
			EventBus.listen("countdown", updateCountdown);
			
			//Updater.getInstance().addListener(this, false);
		}
		
		public function timerUpdate(delta:int):void{}
		
		public function updateCountdown(e:BaseEvent):void {
trace("GOT HERE")
			var eventData:Object = e.getData();
trace("TIME REMAINING", eventData.timeRemaining);			
			m_asset.countDownField.text = String(eventData.timeRemaining);
		
		}		
	}
}