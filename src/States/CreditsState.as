/**
 * @author Daniel Murker
 * 
 * Main splash screen of the game.  Entry point.  start the music!
 */

package States { 
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.SoundManager;
	
	import Events.BaseEvent
	import Events.NetworkEvent;
	import Events.StateMachineEvent;
	
	import Interfaces.StateInterface;
	
	import NetworkCommands.MainSplashStateCommands;
	
	import Utils.Cache;
	import Utils.EventBus;
	import Utils.Promise.Deferred;
	
	public class CreditsState implements StateInterface {
		protected var m_asset:MovieClip;
		
		public function init():Deferred {
			
			var cachePromise:Deferred = Cache.load('/assets/swf/credits.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.Credits'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnBack, backToMain);
			
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			//SoundManager.getInstance().playMusic(SoundManager.MUSIC_MAIN);
		}
		
		public function exit():void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnBack, backToMain);

			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		
		public function timerUpdate(delta:int):void { }
		
		protected function enterGame(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var commandObj:Object = MainSplashStateCommands.getInstance().enterGame();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
			//SoundManager.getInstance().stopMusic();
		}
		

		
	
		protected function backToMain(target:Object):void{
			SoundManager.getInstance().playSfx('click');
			EventBus.dispatch(new StateMachineEvent(StateMachineEvent.STATEMACHINE_CHANGESTATE), {
				statename: "MainMenu"
			});
		}
	}
}