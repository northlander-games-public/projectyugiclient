/**
 * @todo everything
 * @todo 
 * 
 * @author Daniel Murker
 */

package States 
{
	
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import Interfaces.StateInterface;
	import Utils.Promise.Deferred;
	
	import Utils.Cache;
	import Utils.EventBus;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.Updater;
	
	import Events.LobbyStateEvent;
	import GameObjects.gameChunk;
	import GameObjects.gameBlock;
	
	/*import GameObjects.gameBlock*/;

	public class LobbyState 
	{
		protected var m_asset:MovieClip;
		
		protected var m_initialized:Boolean = false;
		protected var m_updateInterval:int = 1000; //milliseconds, I think;
		protected var m_delta:int = 0;
		protected var m_intervalCount:int = 0;
		public const INITIALIZED_EVENT:String = LobbyStateEvent.LOBBY_STATE_INITIALIZED;
		
		
		
		public function LobbyState() {
			//nothing here yet?
		}
		
		public function init():void {
			
			Cache.load('/assets/swf/lobby.swf', continueInit);
		}
		
		/**
		 * this needs to be fixed once the other board becomes active
		 */
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.lobbyBoard'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			EventBus.dispatch(new LobbyStateEvent(INITIALIZED_EVENT));
			
		}
		
		
		public function isInitialized():Boolean {
			return m_initialized;
		}
		
		public function enter(data:Object = null):void {
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			//Updater.getInstance().addListener(this);
			InputManager.getInstance().register(InputManager.INPUT_SEND, sendChat);
		}
		
		/**
		 * clean up event listeners etc.
		 */
		public function exit():void {
			DisplayManager.clearLayer(DisplayManager.GAMELAYER);
			//Updater.getInstance().removeListener(this);
			
		}
		
		public function timerUpdate(delta:int):void {

		}
		
		public function networkUpdate(data:Object = null):void {
//stubbed!
		}
		
		
		/**
		 * INPUT HANDLERS
		 */
		protected function sendChat():void {
			var message:String = m_asset.chatInput.text;
			if (message == "") {
				return;
			}
			m_asset.chatInput.text = "";
			
			message = this.getName() + " >> " + message + "\n";
			
			m_asset.chatLog.text += message;
		}
		
		/**
		 * @todo modify this to pull an identifier from the game object, rather than a chat box. 
		 * @return
		 */
		protected function getName():String {
			return 'foo';
		}
		

	}

}