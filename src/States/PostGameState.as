/**
 * @author Daniel Murker
 * 
 * Main splash screen of the game.  Entry point.  start the music!
 */

package States { 
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import flash.text.TextFieldType;
	
	import Interfaces.StateInterface;
	
	import Utils.Cache;
	import Utils.Promise.Deferred;
	import Utils.EventBus;
	
	import Core.InputManager;
	import Core.DisplayManager;
	import Core.SoundManager;
	
	import Events.NetworkEvent;
	import NetworkCommands.PostGameStateCommands;
	
	public class PostGameState implements StateInterface {
		protected var m_asset:MovieClip;
		
		public function init():Deferred {
			
			var cachePromise:Deferred = Cache.load('/assets/swf/gameOver.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.gameOver'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_asset.p1Portrait.imgLoser.visible = false;
			m_asset.p1Portrait.imgWinner.visible = false;
			m_asset.p2Portrait.imgLoser.visible = false;
			m_asset.p2Portrait.imgWinner.visible = false;
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnBackToMain, backToMain);
			
			if(data.loser){
				m_asset.p1Portrait.imgLoser.visible = true;
				m_asset.p2Portrait.imgWinner.visible = true;
			} else {
				m_asset.p1Portrait.imgWinner.visible = true;
				m_asset.p2Portrait.imgLoser.visible = true;
			}
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			//SoundManager.getInstance().playMusic(SoundManager.MUSIC_MAIN);
		}
		
		public function exit():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btnBackToMain, backToMain);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		
		public function timerUpdate(delta:int):void { }
		
		protected function backToMain(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var commandObj:Object = PostGameStateCommands.getInstance().endGame();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
		}
	}
}