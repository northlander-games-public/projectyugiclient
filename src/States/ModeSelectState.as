/**
 * @author Daniel Murker
 * 
 * Mode Select for playing a game.  :)
 */

package States { 
	import Utils.Promise.Deferred;
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import Interfaces.StateInterface;
	
	import Utils.Cache;
	import Utils.EventBus;
	
	import Events.MainMenuEvent;
	import Events.PopupEvent;
	import Events.NetworkEvent;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.SoundManager;
	
	import Popups.SearchingForOpponentPopup;
	
	public class ModeSelectState implements StateInterface {
		protected var m_asset:MovieClip;
		
		protected var m_initialized:Boolean = false;
		
		public function init():Deferred {
			
			var cachePromise:Deferred = Cache.load('/assets/swf/modeselect.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.modeSelect'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_initialized = true;
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_single, searchForRandomOpponent);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_multiRandom, searchForRandomOpponent);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_multiRanked, searchForRandomOpponent);
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			//SoundManager.getInstance().playMusic(SoundManager.MUSIC_MAIN);
		}
		
		public function exit():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_single, searchForRandomOpponent);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_multiRandom, searchForRandomOpponent);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_multiRanked, searchForRandomOpponent);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		protected function searchForRandomOpponent(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var newPopup:SearchingForOpponentPopup = new SearchingForOpponentPopup();
			newPopup.activate();
		}
		
		public function timerUpdate(delta:int):void { }
		
		
		
		
	}
}