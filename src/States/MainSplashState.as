/**
 * @author Daniel Murker
 * 
 * Main splash screen of the game.  Entry point.  start the music!
 */

package States { 
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import flash.text.TextFieldType;
	
	import Interfaces.StateInterface;
	
	import Utils.Cache;
	import Utils.Promise.Deferred;
	import Utils.EventBus;
	
	import Core.InputManager;
	import Core.DisplayManager;
	import Core.SoundManager;
	
	import Events.NetworkEvent;
	import NetworkCommands.MainSplashStateCommands;
	
	public class MainSplashState implements StateInterface {
		protected var m_asset:MovieClip;
		
		public function init():Deferred {
			
			var cachePromise:Deferred = Cache.load('/assets/swf/mainsplash.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.mainSplash'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_asset.btn_playgame.selectable = false;
			m_asset.btn_playgame.type = TextFieldType.DYNAMIC;
			
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_playgame, enterGame);
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			//SoundManager.getInstance().playMusic(SoundManager.MUSIC_MAIN);
		}
		
		public function exit():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_playgame, enterGame);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		
		public function timerUpdate(delta:int):void { }
		
		protected function enterGame(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var commandObj:Object = MainSplashStateCommands.getInstance().enterGame();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
			//SoundManager.getInstance().stopMusic();
		}
	}
}