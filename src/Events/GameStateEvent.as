/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class GameStateEvent extends BaseEvent 
	{
		public static const GAME_STATE_INITIALIZED:String = "states.gameState.initialized";
		public static const GAME_CHUNK_INITIALIZED:String = "gameObjects.gameChunk.initialized";
		public static const GAME_BLOCK_INITIALIZED:String = "gameObjects.gameBlock.initialized";
		public static const GAME_CHUNK_LIST_REPLENISHED:String = "states.gameState.replenished";
		public static const GAME_CHUNK_ROTATED:String = "gameObjects.gameChunk.rotated";
		
		public static const GAME_START:String = "events.gamestate.start";
		
		public static const SERVER_TILES_SET:String = "serverTilesSet";
		public static const SERVER_CHUNKS_REPLENISHED:String = "events.gamestate.server.chunks.replenished";
		public static const SEEK_PLAYER:String = "events.gamestate.seekplayer";
		public static const MOVE_CURSOR:String =  "updateCursorMoved";
		public static const ROTATE_CURSOR:String = "updateCursorRotated";
		public static const REQUEST_MORE_TILES:String = 'requestMoreTiles';
		public static const LOCK_TILES:String = 'updateLockTiles';
		public static const REMOVE_TILES:String = 'updateRemoveTiles';
		public static const GAME_OVER:String = "gameOver";
		
		public static const GAME_SERVER_ERROR:String = "gameServerError";
		public static const GAME_SERVER_MAP_CHECK:String = "serverMapCheck";
		
		//powers
		public static const POWER_LIGHTNING_STRIKE:String = 'lightningStrike'; //spellbinder lightning strike
		public static const POWER_DARKNESS_BEGINS:String = 'darknessBegin'; //spellbinder darkness attack begins.
		public static const POWER_DARKNESS_ENDS:String = 'darknessEnd'; //spellbinder darkness attack ends.
		public static const POWER_LOCKDOWN:String = 'lockdown'; //warden masterKey
		public static const POWER_UNLOCK:String = 'unlockTiles'; //warden masterKey 
		public static const POWER_WEB_SHOT:String = 'webShot'; //arachnid webshot begins.
		public static const POWER_WEB_SHOT_END:String = 'webShotEnd'; //arachnid ending webshot
		public static const POWER_WEB_STUCK:String = 'webStuck'; //arachnid, stuck duration.
		
		public function GameStateEvent(event:String = null ) 
		{
			super(event);
		}
		
	}

}