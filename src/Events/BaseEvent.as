package Events 
{
	import flash.events.Event;
	
	public class BaseEvent extends Event 
	{
		protected var m_data:Object = {};
		
		public function BaseEvent(event:String = null) {
			super(event);
		}
		
		public function getData():Object {
			return m_data;
		}
		
		public function setData(data:Object):void {
			m_data = data;
		}
		
	}

}