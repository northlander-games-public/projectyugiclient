package Events 
{
	public class StateMachineEvent extends BaseEvent 
	{
		public static const STATEMACHINE_CHANGESTATE:String = "stateChange";
		
		public function StateMachineEvent(event:String = null)
		{
			super(event);
		}
		
	}

}