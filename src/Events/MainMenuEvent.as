/**
 * ...
 * @author Daniel Murker
 */
package Events 
{
	
	public class MainMenuEvent extends BaseEvent {
		public static const MODE_SELECT:String = "enterModeSelect";
		public static const PROFILE:String = "enterProfile";
		public static const SETTINGS:String = "enterSettings";
		
		public function MainMenuEvent(event:String = null ) {
			super(event);
		}
		
	}
}