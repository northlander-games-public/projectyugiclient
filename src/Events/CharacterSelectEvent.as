/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class CharacterSelectEvent extends BaseEvent 
	{
		
		public static const CHARACTER_SELECTED:String = "characterSelected";
		public static const CHARACTER_ACKNOWLEDGED:String = "characterAcknowledged";
		public static const POWER_SELECTED:String = "powerSelected";
		public static const POWER_ACKNOWLEDGED:String = "powerAcknowledged";
		public static const BEGIN_COUNTDOWN:String = "beginCountdown";
		public static const COUNTDOWN_COMPLETE:String = "countdownComplete";
		
		public function CharacterSelectEvent(event:String = null ) 
		{
			super(event);
		}
		
	}

}