/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class PostGameEvent extends BaseEvent 
	{
		public static const BACK_TO_MAIN:String = 'backToMain';
		
		public function PostGameEvent(event:String = null ) 
		{
			super(event);
		}
	}
}