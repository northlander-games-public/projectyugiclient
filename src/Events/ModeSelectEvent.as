/**
 * ...
 * @author Daniel Murker
 */
package Events 
{
	
	public class ModeSelectEvent extends BaseEvent {
		public static const SEARCH_FOR_OPPONENT:String = "searchForOpponent";
		public static const OTHER_PLAYER_FOUND:String = "otherPlayerFound";
		public static const ACCEPT_MATCH:String = "acceptMatch";
		
		public function ModeSelectEvent(event:String = null ) {
			super(event);
		}
		
	}
}