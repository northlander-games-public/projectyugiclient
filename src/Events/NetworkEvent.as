/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class NetworkEvent extends BaseEvent 
	{
		public static const SOCKET_CONNECTED:String = "networkEvent.socketConnected";
		
		public static const SOCKET_DATA_RECEIVED:String = "networkEvent.socketDataReceived";
		public static const SOCKET_ERROR_RECEIVED:String = "networkEvent.socketErrorReceived";
		
		public static const SOCKET_DATA_WRITE:String = "networkEvent.socketDataWrite";
		
		public static const CREATE_NEW_CONNECTION:String = 'createNewConnection';
		
		public function NetworkEvent(event:String = null ) 
		{
			super(event);
		}
		
	}

}