/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class MainSplashEvent extends BaseEvent 
	{
		public static const ENTER_GAME:String = 'enterGame';
		
		public function MainSplashEvent(event:String = null ) 
		{
			super(event);
		}
	}
}