package GameObjects 
{
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import Utils.Cache;
	
	public class gameCursor
	{
		private var m_asset:MovieClip;
		//private var m_data:Object;
		
		public function gameCursor() {
			Cache.load('/assets/swf/gameboard.swf', init);
		}
		
		protected function init():void {
			m_asset = new (getDefinitionByName('assets.inputCursor'))();
			
			m_asset.z = 1;
			
			/*EventBus.dispatch(new GameStateEvent(GameStateEvent.GAME_BLOCK_INITIALIZED), this);
			m_isInitialized = true;*/
		}
		
		public function getAsset():MovieClip {
			return m_asset;
		}
	}
	
}