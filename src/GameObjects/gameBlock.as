package GameObjects
{
	/**
	 * @todo figure out how to handle block generation en masse for garbage drops.
	 * @todo this object should sort of 'build itself'.  the game chunk should have no idea what its holding, or what the result of a drop will be.
	 */
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import Math;
	import Utils.Cache;
	import Utils.EventBus;
	
	import Events.GameStateEvent;
	
	public class gameBlock
	{
		protected var m_value:int = 1;
		protected var m_asset:MovieClip;
		
		//protected var m_colors:Array = ['blue', 'green', 'yellow', 'red'];
		protected var m_chosenColor:String;
		protected var m_chosenPower:String;
		
		protected var m_isInitialized:Boolean = false;
		protected var m_isLocked:Boolean = false;
		protected var m_isMarked:Boolean = false;
		
		/**
		 * @todo modify this to allow for the creation of 'igniters' and 'diamonds' (possibly creation of a behavior system?)
		 */
		public function gameBlock(color:String) {
			var colorChunks:Array = color.split('_');
			m_chosenColor = colorChunks[0];
			if (colorChunks.length == 2) {
				m_chosenPower = colorChunks[1];
trace("CHOSEN POWER FOUND!  WOOOOOOO! " + m_chosenColor + " " + m_chosenPower);
			}
			
			Cache.load('/assets/swf/gameboard.swf', init);
		}
		
		/**
		 * @todo modify this to break power tiles out of the chosen color
		 */
		protected function init():void {
			var whichTile:String = 'assets.hexTile.' + m_chosenColor;
			m_asset = new (getDefinitionByName(whichTile));
			
			/*var randomColor:int = Math.floor(Math.random() * m_colors.length);
			m_chosenColor = m_colors[randomColor];*/
			
			//m_asset.gotoAndStop(m_chosenColor);
			m_asset.z = 0;
			
			EventBus.dispatch(new GameStateEvent(GameStateEvent.GAME_BLOCK_INITIALIZED), this);
			
			m_isInitialized = true;
		}
		
		public function isInitialized():Boolean {
			return this.m_isInitialized;
		}
		
		public function getAsset():MovieClip {
			return m_asset;
		}
		
		public function getColor():String {
			return m_chosenColor;
		}
		
		/*public function setColor(newColor:String):void {
			m_chosenColor = newColor;
			m_asset.gotoAndStop(m_chosenColor);
		}*/
		
		public function markForDeletion():void {
			m_isMarked = true;
		}
		
		/**
		 * absolutely no clue how I'm implementing this yet, if at all.  basically
		 * my version of merging at least 4 gems into a power gem... 
		 */
		protected function upgrade():void {
			
		}
		
		/**
		 * advance the block to a frame in which it animates its exit by either 
		 */
		protected function send():void {
			
		}
		
		public function lock():void {
			m_isLocked = true;
		}
		
		public function unlock():void { 
			m_isLocked = false;
		}
		
		public function isLocked():Boolean {
			return m_isLocked;
		}
	}
}