package Core {
	import Events.BaseEvent;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	
	import flash.utils.getDefinitionByName;
	import Utils.Cache;
	
	import Utils.Promise.Deferred;
	
	public class SoundManager {
		private static var s_instance:SoundManager;
		
		private var m_musicEnabled:Boolean = true;
		private var m_sfxEnabled:Boolean = true;
		
		private var m_currentMusicChannel:SoundChannel;
		private var m_currentMusic:Sound;
		
		private var m_currentSfxChannel:SoundChannel;
		private var m_currentSfx:Sound;
	
		private var m_soundPromise:Deferred;
		private var m_initialized:Boolean = false;
		
		public static const MUSIC_MAIN:String = 'main';
		public static const MUSIC_BATTLE:String = 'battle';
		
		public function SoundManager(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO SOUND MANAGER");
			}
		}
		
		public static function getInstance():SoundManager {
			if (!s_instance) {
				s_instance = new SoundManager(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function init():void {
			//var soundsJson:Object = EmbeddedData.getData(EmbeddedData.SOUNDS);
			m_soundPromise = Cache.load("/assets/swf/sounds.swf");
			
			m_soundPromise.then(function():void {
				m_initialized = true;
			});
		}
		
		public function playMusic(songName:String = MUSIC_MAIN, loopForever:Boolean = true):void {
			if (!m_musicEnabled) {
				return; 
			}
			
			if (!m_initialized) {
				m_soundPromise.then(function():void {
					playMusic(songName, loopForever);
				});
				return;
			}
			
			
			m_currentMusic = new (getDefinitionByName('music.' + songName))();
			//if we're changing songs, kill the original music.
			if (m_currentMusicChannel) {
				m_currentMusicChannel.stop();
			}
			m_currentMusicChannel = m_currentMusic.play();
			if (loopForever && !m_currentMusicChannel.hasEventListener(Event.SOUND_COMPLETE)) {
				m_currentMusicChannel.addEventListener(Event.SOUND_COMPLETE, m_currentMusic.play);
			}	
		}

		public function stopMusic():void {
			m_currentMusicChannel.stop();
			m_currentMusicChannel.removeEventListener(Event.SOUND_COMPLETE, m_currentMusic.play);
		}
		
		public function disableMusic():void {
			m_musicEnabled = false;
			stopMusic();
		}
		
		public function enableMusic():void {
			m_musicEnabled = true;
			playMusic();
		}
		
		public function playSfx(sfxName:String):void {
			if (!m_sfxEnabled) {
				return; 
			}
			
			if (!m_initialized) {
				m_soundPromise.then(function():void {
					playSfx(sfxName);
				});
				return;
			}
			
			m_currentSfx = new (getDefinitionByName('sfx.' + sfxName))();
			if (m_currentSfxChannel) {
				m_currentSfxChannel.stop(); //this could be temporary, not sure how I feel about overlapping sfx.
			}
			m_currentSfxChannel = m_currentSfx.play();
		}
		
		public function stopSfx():void {
			m_currentSfxChannel.stop();
		}
		
		public function disableSfx():void {
			stopSfx();
			m_sfxEnabled = false;
		}
		
		public function enableSfx():void {
			m_sfxEnabled = true;
		}
	}
}

class SingletonEnforcer {}