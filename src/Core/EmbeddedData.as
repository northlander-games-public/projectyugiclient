/**
 */

package Core 
{
	import flash.utils.getDefinitionByName;
	import JSON;
	
	public class EmbeddedData 
	{
		private static var s_data:Object = {};
		
		/**
		 * Game States Data
		 */
		[Embed (source = "../../assets/data/states.json", mimeType = "application/octet-stream")]
		private static var m_gameStates:Class;
		
		public static const GAMESTATES:String = "embeddedData.GameStates";
		
		/**
		 * Display Layers Data
		 */
		[Embed (source = "../../assets/data/layers.json", mimeType = "application/octet-stream")]
		private static var m_gameLayers:Class;
		
		public static const GAMELAYERS:String = "embeddedData.GameLayers";
		
		/**
		 * Default Keybindings Data
		 */
		[Embed (source = "../../assets/data/keybindings.json", mimeType = "application/octet-stream")]
		private static var m_keybindings:Class;
		
		public static const KEYBINDINGS:String = "embeddedData.Keybindings";
		
		/**
		 * Network connection data
		 */
		[Embed (source = "../../assets/data/network.json", mimeType = "application/octet-stream")]
		private static var m_network:Class;
		
		public static const NETWORK:String = "embeddedData.Network";
		
		/**
		 * Sounds data
		 */
		[Embed (source = "../../assets/data/sounds.json", mimeType = "application/octet-stream")]
		private static var m_sounds:Class;
		
		public static const SOUNDS:String = "embeddedData.Sounds";
		
		
		/**
		 * constructor
		 */
		public function EmbeddedData(key:SingletonEnforcer = null) {
			if ( key == null ){
				throw new Error("NO! BAD CODER!");
			}
		}
		
		public static function init():void {
		
			var initDictionary:Object = { };
			initDictionary[GAMESTATES] = m_gameStates;
			initDictionary[GAMELAYERS] = m_gameLayers;
			initDictionary[KEYBINDINGS] = m_keybindings;
			initDictionary[NETWORK] = m_network;
			initDictionary[SOUNDS] = m_sounds;
			
			for (var i:String in initDictionary) {
				if (initDictionary[i]) {
					var temp:String = new (initDictionary[i])().toString();
					s_data[i] = JSON.parse(temp);
					initDictionary[i] = null;
				}
				
			}
		}
		
		public static function getData(key:String):Object {
			if (!s_data.hasOwnProperty(key)) {
				throw new Error("BAD MONKEY! NO DATA!");
			}
			
			return s_data[key];
		}
		
		/*public static function getData(s:String):Object
		{
			return m_gameStates;
			var klass:Class = getDefinitionByName(s);
			var result:Object = new k();
			return result;
		}*/
		
		
		
	}

}

class SingletonEnforcer
{
}