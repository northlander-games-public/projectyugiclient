/**
 * ...
 * @author ...
 */

package Core 
{
	import States.*;
	
	public class DynamicClasses 
	{
		
		public function DynamicClasses() {}
		
		public static function init():void { }
		
		private static const STATECLASSES:Array = [
			MainSplashState,
			MainMenuState,
			ModeSelectState,
			CharacterSelectState,
			GameState,
			PostGameState,
			SettingsState,
			CreditsState
		];
	}

}