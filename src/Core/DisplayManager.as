package Core 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import Core.EmbeddedData;
	import flash.system.Capabilities;
	
	
	public class DisplayManager 
	{
		protected static var s_layers:Object;
		
		protected static var s_rootElement:Sprite;
		
		public static const GAMELAYER:String = "GameLayer";
		public static const POPUPLOWLAYER:String = "PopupLowLayer";
		public static const HUDLAYER:String = "HudLayer";
		public static const POPUPHIGHLAYER:String = "PopupHighLayer";
		
		public function DisplayManager(){
			
		}
		
		public static function init(rootElement:Sprite):void {
			if (s_rootElement) {
				throw new Error("ROOT ELEMENT HAS ALREADY BEEN SET!  BAD MONKEY!");
			}
			s_layers = { };
			s_rootElement = rootElement;
			
			var layersJson:Object = EmbeddedData.getData(EmbeddedData.GAMELAYERS);
			
			for (var i:String in layersJson['LayersList']) {
				var layerName:String = layersJson['LayersList'][i];
				
				s_layers[layerName] = new Sprite();
				s_rootElement.addChild(s_layers[layerName]);
			}
			
			getDefaultResolution();
		}
		
		public static function addToDisplay(asset:Sprite, layer:String):void {
			s_layers[layer].addChild(asset);
		}
		
		public static function removeFromDisplay(asset:Sprite, layer:String):void {
			s_layers[layer].removeChild(asset);
		}
		
		//do not use?
		public static function clearLayer(layer:String):void {
			var numChildren:int = s_layers[layer].numChildren;
			//s_layers[layer].removeChildren(0, numChildren - 1);


		}
		
		public static function getDefaultResolution():void {
			var resolution:String = Capabilities.screenResolutionX as String;
			resolution += "x";
			resolution += Capabilities.screenResolutionY as String;
			trace(resolution);		
		}
		
		
		
	}

}