/**
 * This doubles as a state factory, generating the states when it's first being created.
 * @author Daniel Murker
 */


package Core 
{
	import adobe.utils.CustomActions;
	import Events.BaseEvent;
	import flash.utils.getDefinitionByName;
	import Interfaces.StateInterface;
	
	import Core.EmbeddedData;
	import Core.EmbeddedData;
	import Utils.EventBus;
	import Events.NetworkEvent;
	import Events.StateMachineEvent;
	import NetworkCommands.StateMachineCommands;
	
	public class GameStateMachine
	{
		/**
		 * struct keyed by state names, holding onto classes.
		 */
		protected static var m_statesList:Object = { };
		protected static var m_statePromises:Object = { };
		
		/**
		 * name of a default state
		 */
		protected static var m_defaultState:String;
		
		/**
		 * name of current state
		 */
		protected static var m_currentState:String;
		
		private static const STATEPACKAGE:String = "States";
		
		
		
		
		/**
		 * not sure what should happen in constructor yet, if anything. possibly get the states list based on data.
		 */
		public function GameStateMachine(key:SingletonEnforcer = null) {
			if (!key) {
				throw new Error ("BAD MONKEY!  NO STATE MACHINE!");
			}
		
		}
		
		public static function init():void {
			var states:Object = EmbeddedData.getData(EmbeddedData.GAMESTATES);
			
			for (var i:String in states) {
				var className:String = STATEPACKAGE + '.' + states[i]['className'];
				var classDef:Class = getDefinitionByName(className) as Class;
				
				//var classDef:Class = GameState;
				if (states[i].hasOwnProperty('default') && states[i]['default'] == true) {
					m_defaultState = i;
				}
				m_statesList[i] = new classDef();
				m_statePromises[i] = m_statesList[i].init();
			}
			
			
			
			//should probably have a fail condition just in case...  --DM
			m_statePromises[m_defaultState].then(function():void {
				enterState(m_defaultState);
			});
			
			EventBus.listen(StateMachineEvent.STATEMACHINE_CHANGESTATE, handleStateChange);
		}
		
		protected static function handleStateChange(event:BaseEvent):void {
			var data:Object = event.getData();
			enterState(data.statename, data.data);
		}
		
		public static function enterState(statename:String, data:Object = null):void{
			var newState:* = m_statesList[statename];
			
			if (m_currentState) {
				m_statesList[m_currentState].exit();
			}
			newState.enter(data);
			m_currentState = statename;
			
			/**
			 * tell the network manager we're changing states.
			 */
			var netEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			var command:Object = StateMachineCommands.getInstance().enterState(statename, data);
			EventBus.dispatch(netEvent, command); 
				
		}
		
	}
}

class SingletonEnforcer {}
