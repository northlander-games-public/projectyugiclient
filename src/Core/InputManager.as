/**
 * @todo connect the keyboard events to the subscribed callbacks, say, from the game state, and ensure they fire properly (it would suck if I had to use the stage for all inputs)
 * @todo leave room for the possibility of remapping data.
 */

package Core 
{	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	
	import Utils.EventBus;
	import Core.EmbeddedData;
	
	
	public class InputManager extends EventDispatcher
	{
		private static var s_instance:InputManager;
		
		private var m_paused:Boolean;
		private var m_keyBindings:Object;
		private var m_reverseKeyBindings:Object;
		private var m_callbacks:Object;
		/**
		 * @todo move these into an input event object before it's too late!
		 */
		public static const INPUT_UP:String = 'Up';
		public static const INPUT_DOWN:String = 'Down';
		public static const INPUT_LEFT:String = 'Left';
		public static const INPUT_RIGHT:String = 'Right';
		public static const INPUT_ROTATE_LEFT:String = 'Rotate Left';
		public static const INPUT_ROTATE_RIGHT:String = 'Rotate Right';
		public static const INPUT_CONFIRM:String = 'Confirm/Drop';
		public static const INPUT_SEND:String = 'Send';
		
		/**
		 * @todo mouse input needs to be handled better?
		 */
		public static const MOUSE_CLICK:String = 'mouseClick';
		//public static const MOUSE_UP:String = 'mouseUp';
		
		public function InputManager(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO INPUT MANAGER");
			}
			
			m_callbacks = { };
		}
		
		public static function getInstance():InputManager {
			if (!s_instance) {
				s_instance = new InputManager(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function init():void {
			EventBus.listen(KeyboardEvent.KEY_UP, handleKeyUp);
			EventBus.listen(KeyboardEvent.KEY_DOWN, handleKeyDown);
			EventBus.listen(MouseEvent.CLICK, handleClick);
			//EventBus.listen(MouseEvent.MOUSE_UP, handleMouseUp);
	
			var keyBindingsJson:Object = EmbeddedData.getData(EmbeddedData.KEYBINDINGS);
			m_keyBindings = keyBindingsJson['DefaultBindings'];
			m_reverseKeyBindings = { };
			for (var i:String in m_keyBindings) {
				for (var j:String in m_keyBindings[i]) {
					m_reverseKeyBindings[m_keyBindings[i][j]] = i;
				}
			}
		}
		
		public function pause():void{
			m_paused = true;
		}

		public function unpause( ):void{
			m_paused = false;
		}
		
		public function isPaused():Boolean {
			return m_paused;
		}

		public function register(key:String, callback:Function, onKeyUp:Boolean = false ):void {
			var keySwitch:String = !onKeyUp ? KeyboardEvent.KEY_DOWN : KeyboardEvent.KEY_UP;
			if (!m_callbacks.hasOwnProperty(keySwitch)) {
				m_callbacks[keySwitch] = { };
			}
			if (!m_callbacks[keySwitch].hasOwnProperty(key)) {
				m_callbacks[keySwitch][key] = [];
			}
			
			m_callbacks[keySwitch][key].push(callback);
			
		}

		public function unregister(key:String, callback:Function ):void{
			var keyDownIndex:int = m_callbacks[KeyboardEvent.KEY_DOWN][key].indexOf(callback);
			//var keyUpIndex:int = m_callbacks[KeyboardEvent.KEY_UP][key].indexOf(callback);
			if (~keyDownIndex) {
				m_callbacks[KeyboardEvent.KEY_DOWN][key].splice(keyDownIndex, 1);
			}
			
			/*if (~keyUpIndex) {
				m_callbacks[KeyboardEvent.KEY_UP][key].splice(keyUpIndex, 1);
			}*/
		}
		
		public function registerMouse(key:String, target:Object, callback:Function):void {
			if (!target['name']) {
				target.name = 'defaultName';
			}
			if (!m_callbacks.hasOwnProperty(MouseEvent.CLICK)) {
				m_callbacks[MouseEvent.CLICK] = { };
			}
			if (!m_callbacks[MouseEvent.CLICK].hasOwnProperty(target.name)) {
				m_callbacks[MouseEvent.CLICK][target.name] = [];
			}
			m_callbacks[MouseEvent.CLICK][target.name].push(callback);
		}
		
		public function unregisterMouse(key:String, target:Object, callback:Function):void {
			for (var targetName:String in m_callbacks[MouseEvent.CLICK]) {
				if (targetName != target.name) {
					continue;
				}
				
				var cbIndex:int = m_callbacks[MouseEvent.CLICK][targetName].indexOf(callback);
				if (~cbIndex) {
					m_callbacks[MouseEvent.CLICK][targetName].splice(cbIndex, 1);
				}
			}
		}
		
		protected function handleKeyDown(e:KeyboardEvent):void {
			var key:String = m_reverseKeyBindings[e.keyCode];
			var event:String = KeyboardEvent.KEY_DOWN;
			handleKeyboardInputEvent(key, event);
		}
		
		protected function handleKeyUp(e:KeyboardEvent):void {
			var key:String = m_reverseKeyBindings[e.keyCode];
			var event:String = KeyboardEvent.KEY_UP;
			handleKeyboardInputEvent(key, event);
		}
		
		protected function handleClick(e:MouseEvent):void {
			var key:String = MOUSE_CLICK;
			var event:String = MouseEvent.CLICK;
			handleMouseInputEvent(key, (e.target), event);
		}
		
		/*protected function handleMouseUp(e:MouseEvent):void {
			var key:String = MOUSE_UP;
			var event:String = MouseEvent.MOUSE_UP;
			handleMouseInputEvent(event);
		}*/
		
		protected function handleKeyboardInputEvent(key:String, event:String):void {
			if (m_paused) {
				return;
			}
			if (m_callbacks.hasOwnProperty(event) && m_callbacks[event].hasOwnProperty(key)) {
				for (var i:* in m_callbacks[event][key])
				{
					m_callbacks[event][key][i](key); //god, trying to read this hurts MY eyes.  --DM
				}
			}
		}
		protected function handleMouseInputEvent(key:String, target:Object, event:String):void {
			if (m_paused) {
				return;
			}
			
			if (m_callbacks.hasOwnProperty(event)) {
				
				if (m_callbacks[event].hasOwnProperty(target.name)) {
					for (var i:* in m_callbacks[event][target.name]){
						m_callbacks[event][target.name][i](target); //yay copypasta.  --DM
					}
				} else if(target.parent){
					handleMouseInputEvent(key, target.parent, event);
				}
			
			}
		}
		
	}
}

class SingletonEnforcer {}