/**
 * @todo generate state machine with two states (a lobby and a game)
 * @todo enter the menu state.
 * @todo ensure the menu state has a button that triggers entry into the game state
 * @todo ensure the game state has a button that triggers entry into the menu state
 * @todo generate code in the game state for creating a chunk out of random pieces (data driven weights?)
 * @todo create a button that randomly generates that chunk
 * @todo create an input handler the chunk listens to such that it can be rotated and moved.
 * @todo put the chunk on a board with boundaries.
 * 
 * @author Daniel Murker
 */

package  
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.system.Security;
	
	import Core.DisplayManager;
	import Core.DynamicClasses;
	import Core.EmbeddedData;
	import Core.GameStateMachine;
	import Core.InputManager;
	import Core.NetworkManager;
	import Core.SoundManager;
	import Core.Updater;
	
	import Events.BaseEvent;
	
	import Utils.EventBus;
	
	
	
	[SWF( width = '800', height = '450' )] //this will probably change.
	public class ProjectYugi extends Sprite
	{
		
		public function ProjectYugi():void {
			
			//Security.allowDomain( "*" );
			
			EventBus.init(stage);
			EmbeddedData.init();
			NetworkManager.getInstance().init();
			DynamicClasses.init();
			InputManager.getInstance().init();
			Updater.getInstance().init();
			SoundManager.getInstance().init();
			
			this.addEventListener( Event.ADDED_TO_STAGE, _init );
			
			EventBus.listen('FULLSCREEN', enterFullScreen);
			EventBus.listen('NORMAL', exitFullScreen);
		}
		
		protected function _init(e:Event):void {
			// if I'm using a loading screen, I want to draw this behind while loading is taking place.
			if( parent is Loader ){					
				parent.parent.addChildAt(this, 0);
			}
			DisplayManager.init(this);
			GameStateMachine.init();
			
		}

		protected function enterFullScreen(event:BaseEvent):void{
			stage.displayState = StageDisplayState.FULL_SCREEN;
		}
		
		protected function exitFullScreen(event:BaseEvent):void{
			stage.displayState = StageDisplayState.NORMAL;
		}
	}

}