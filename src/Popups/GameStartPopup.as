package Popups {
	import flash.text.Font;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	
	import Core.Updater;
	
	import Events.BaseEvent;
	import Events.GameStateEvent;
	import Events.NetworkEvent;
	
	import NetworkCommands.CharacterSelectStateCommands;
	
	import Utils.EventBus;
	
	
	
	public class GameStartPopup extends BasePopup {
		protected var m_textFormat:TextFormat;
		
		protected var m_delta:int;
		protected var m_updateInterval:int;
		protected var m_intervalCount:int;
		
		public function GameStartPopup() 
		{	
			super();	
		}
		
		override public function activate():void {
			if (!m_initialized) {
				m_deferred.then(activate);
				return;
			}
			
			m_delta = 0;
			m_updateInterval = 1000;
			m_intervalCount = 5;
			
			m_asset = new (getDefinitionByName('assets.gameStartPopup'))();
			m_asset.msgCountdown.embedFonts = false;
			//m_asset.msgCountdown.defaultTextFormat.font
			//m_textFormat = m_asset.msgCountdown.getTextFormat();
			
			
			
			//super adds to stage etc.
			super.activate();
			//m_asset.msgCountdown.border = true;
			m_asset.msgCountdown.text = m_intervalCount;
			
			//m_asset.msgCountdown.defaultTextFormat = m_textFormat;
			//startCountdown();
			EventBus.listen("countdown", updateCountdown);
		}
		
		/**
		 * @todo think about creating a system that keeps track of server time and client time.
		 */
		protected function updateCountdown(e:BaseEvent):void {
			//Updater.getInstance().addListener(this, false);
			var eventData:Object = e.getData();
			
			m_asset.msgCountdown.text = String(eventData.timeRemaining);
						
			if (eventData.timeRemaining <= 0) {
				EventBus.stopListening("countdown", updateCountdown);
				EventBus.dispatch(new GameStateEvent(GameStateEvent.GAME_START));
				super.deactivate();
			}
		}
		
		override public function timerUpdate(delta:int):void {
			
		}
	}
}