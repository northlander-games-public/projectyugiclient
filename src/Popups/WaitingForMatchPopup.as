package Popups 
{
	
	import flash.utils.getDefinitionByName;
	
	import Utils.EventBus;
	import Core.Updater;
	
	import Events.BaseEvent;
	import Events.PopupEvent;
	import Events.GameStateEvent;
	import Events.NetworkEvent;
	import Events.StateMachineEvent;
	
	import NetworkCommands.GameStateCommands;
	import NetworkCommands.StateMachineCommands;
	
	
	public class WaitingForMatchPopup extends BasePopup 
	{
		protected var m_delta:int;
		protected var m_updateInterval:int;
		protected var m_intervalCount:int;
		
		public function WaitingForMatchPopup() 
		{
			m_delta = 0;
			m_intervalCount = 0;
			m_updateInterval = 1000;
			m_intervalCount = 3;
			
			super();
		}
		
		override protected function init():void {
			m_asset = new (getDefinitionByName("assets.popupWaiting"))();
			m_asset.stop();
			
			EventBus.listen(PopupEvent.OTHER_PLAYER_FOUND, handleMatchFound);
			EventBus.dispatch(new PopupEvent(PopupEvent.POPUP_INITIALIZED), this);
		}
		
		override public function activate():void {
			super.activate();
			
			//this is a bit of a hack because there's no true matchmaking state on the client yet.
			var commandObj:Object = StateMachineCommands.getInstance().enterState('MatchMaking');
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			
			EventBus.dispatch(networkEvent, commandObj);
		}
		
		/**
		 * @todo think about creating a system that keeps track of server time and client time.
		 */
		protected function handleMatchFound(e:BaseEvent):void {
			EventBus.dispatch(new StateMachineEvent(StateMachineEvent.STATEMACHINE_CHANGESTATE), { statename: "Game", data: e.getData()});
			deactivate();
		}
		/*protected function startCountdown(e:BaseEvent):void {
			Updater.getInstance().addListener(this, false);
		}*/
		
		/*override public function timerUpdate(delta:int):void {
			m_delta += delta;
			if (m_delta < m_updateInterval){
				return;
			}
			m_delta = 0;
			
			if (m_intervalCount == 0) {
				Updater.getInstance().removeListener(this);
				EventBus.stopListening(PopupEvent.OTHER_PLAYER_FOUND, startCountdown);
				EventBus.dispatch(new GameStateEvent(GameStateEvent.GAME_START));
				deactivate();
			}
			else {
				m_asset.gotoAndStop("countdown_" + (m_intervalCount--));
			}
			
		}*/
		
	}

}