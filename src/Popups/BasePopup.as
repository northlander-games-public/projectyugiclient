package Popups 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;
	
	import Interfaces.UpdaterInterface;
	import Utils.Cache;
	import Events.PopupEvent;
	import Core.DisplayManager;
	
	import Utils.Promise.Deferred;
	
	public class BasePopup implements UpdaterInterface
	{
		protected var m_deferred:Deferred;
		protected var m_initialized:Boolean;
		
		protected var m_modal:Boolean;
		protected var m_important:Boolean;
		
		protected var m_asset:MovieClip;
		
		protected static var s_numActiveModalPopups:int;
		
		public function BasePopup() {
			m_deferred = Cache.load('/assets/swf/popups.swf');
			m_deferred.then(function():void {
				m_initialized = true;
			});
		}
		
		protected function init():void {
		}
		
		public function setModality(modal:Boolean):void {
			m_modal = modal;
		}
		
		public function setImportance(important:Boolean = false):void {
			m_important = important;
		}
		
		public function activate():void {
			switch(m_important) {
				case true:
					DisplayManager.addToDisplay(m_asset, DisplayManager.POPUPHIGHLAYER);
					break;
				case false:
					DisplayManager.addToDisplay(m_asset, DisplayManager.POPUPLOWLAYER);
					break;
			}
			
			
			/**
			 * if this popup is modal, then the first child in the display layer should be 
			 */
			if (m_modal) {
				if (!s_numActiveModalPopups) {
					s_numActiveModalPopups = 0;
				}

				++s_numActiveModalPopups;
				
				//if (!(firstLayer is bgAssetType)) {
				if(s_numActiveModalPopups == 1){
					var bgAssetType:Class = (getDefinitionByName("assets.popupBgModal") as Class);
					m_asset.parent.addChildAt(new bgAssetType, 0);
				}
			}
		}
		
		public function deactivate():void {
			switch(m_important) {
				case true:
					DisplayManager.removeFromDisplay(m_asset, DisplayManager.POPUPHIGHLAYER);
					break;
				case false:
					DisplayManager.removeFromDisplay(m_asset, DisplayManager.POPUPLOWLAYER);
					break;
			}
			
			if (m_modal) {
				--s_numActiveModalPopups;
				
				if (s_numActiveModalPopups == 0) {
					//THIS ASSUMES A MODAL BG IS AT THE 0 INDEX!  please do not screw with this assumption.
					m_asset.parent.removeChildAt(0);
				}
			}
		}
		
		public function timerUpdate(delta:int):void {
			//stubbed.  child classses should implement this if they attach to the updater.
		}
	}
}