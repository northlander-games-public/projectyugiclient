package Popups {
	
	import flash.display.MovieClip;
	import NetworkCommands.ModeSelectStateCommands;
	import Events.ModeSelectEvent;
	import Events.StateMachineEvent;
	import Events.BaseEvent;
	import Events.NetworkEvent;
	
	import Utils.EventBus;
	
	import Core.InputManager;
	
	import flash.utils.getDefinitionByName;
	
	public class SearchingForOpponentPopup extends BasePopup {
		public function SearchingForOpponentPopup() {
			super();
		}
		
		override public function activate():void {
			if (!m_initialized) {
				m_deferred.then(activate);
				return;
			}
			
			m_asset = new (getDefinitionByName('assets.popupSearchingForOpponent'))();
			m_asset.msgWaiting.visible = false;
			m_asset.msgFound.visible = false;
			m_asset.btn_accept.visible = false;
			m_asset.btn_cancel_2.visible = false;
			
			
			EventBus.listen(ModeSelectEvent.OTHER_PLAYER_FOUND, otherPlayerFound);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_accept, confirmMatch);
			
			//tell the server to begin looking for a match.
			var commandObj:Object = ModeSelectStateCommands.getInstance().searchForOpponent('random');
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			
			EventBus.dispatch(networkEvent, commandObj);
			
			//super adds to stage etc.
			super.activate();
		}
		
		override public function deactivate():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_accept, confirmMatch);
			
			super.deactivate();
		}
		
		protected function otherPlayerFound(event:BaseEvent):void {
			
			m_asset.msgSearching.visible = false;
			m_asset.btn_cancel_1.visible = false;
			
			m_asset.msgFound.visible = true;
			m_asset.btn_accept.visible = true;
			m_asset.btn_cancel_2.visible = true;
		}
		
		protected function confirmMatch(target:Object):void {
			m_asset.msgFound.visible = false;
			m_asset.btn_accept.visible = false;
			m_asset.btn_cancel_2.visible = false;
			
			
			m_asset.msgWaiting.visible = true;
			
			var commandObj:Object = ModeSelectStateCommands.getInstance().acceptMatch();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
			
			
			//listen for state change event to kill this popup.
			EventBus.listenOnce(StateMachineEvent.STATEMACHINE_CHANGESTATE, handleStateChange);
		}
		
		protected function handleStateChange(event:BaseEvent):void {
			var data:Object = event.getData();
			
			if (data.statename == 'CharacterSelect') {
				this.deactivate();
			}
		}
	}
}