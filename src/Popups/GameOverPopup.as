package Popups 
{
	
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import Core.InputManager;
	
	import Events.BaseEvent;
	import Events.GameStateEvent;
	import Events.NetworkEvent;
	import Events.StateMachineEvent;
	
	import NetworkCommands.GameStateCommands;
	
	import Utils.EventBus;
	
	public class GameOverPopup extends BasePopup 
	{
		
		protected var m_loser:Boolean;
		
		public function GameOverPopup() 
		{
			super();
		}
		
		override public function activate():void {
			if (!m_initialized) {
				m_deferred.then(activate);
				return;
			}
			
			m_asset = new (getDefinitionByName('assets.gameOverPopup'))();
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btn_gameOverOk, exitGame);
			
			//super adds to stage etc.
			super.activate();
			
			InputManager.getInstance().unpause();
		}
		
		override public function deactivate():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btn_gameOverOk, exitGame);
			
			super.deactivate();
		}
		
		public function setLoserState(isLoser:Boolean):void {
			this.m_loser = isLoser;
		}
		
		protected function exitGame(target:Object):void {			
			var commandObj:Object = GameStateCommands.getInstance().endGame(this.m_loser);
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
			
			
			//listen for state change event to kill this popup.
			EventBus.listenOnce(StateMachineEvent.STATEMACHINE_CHANGESTATE, handleStateChange);
		}
		
		protected function handleStateChange(event:BaseEvent):void {
			var data:Object = event.getData();
			
			if (data.statename == 'PostGame') {
				this.deactivate();
			}
		}
		
		
		
	}

}